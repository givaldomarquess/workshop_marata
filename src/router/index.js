import Vue from 'vue'
import Router from 'vue-router'
import Cronograma from '@/components/Cronograma'
import Buefy from 'buefy'
import 'buefy/lib/buefy.css'

Vue.use(Buefy, {
  defaultIconPack: 'fa'
});

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Cronograma',
      component: Cronograma
    }
  ]
})
